%{?mingw_package_header}

#define _version_suffix -e198

Name:           mingw-qemu-ga
Version:        2.10.1
Release:        1%{?dist}
Summary:        MinGW Windows QEMU guest agent

License:        GPLv2+ and LGPLv2+ and BSD
URL:            http://www.qemu.org
Source0:        https://download.qemu.org/qemu-%{version}.tar.xz

BuildRequires:  mingw32-filesystem >= 23
BuildRequires:  mingw64-filesystem >= 23
BuildRequires:  mingw32-gcc
BuildRequires:  mingw64-gcc
BuildRequires:  mingw32-gcc-c++
BuildRequires:  mingw64-gcc-c++
BuildRequires:  mingw32-glib2
BuildRequires:  mingw64-glib2
BuildRequires:  mingw32-pkg-config
BuildRequires:  mingw64-pkg-config
BuildRequires:  mingw32-zlib-static
BuildRequires:  mingw64-zlib-static
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  msitools
BuildRequires:  python

BuildArch:      noarch

%package -n mingw32-qemu-ga
Summary:        MinGW Windows QEMU guest agent

%package -n mingw64-qemu-ga
Summary:        MinGW Windows QEMU guest agent

%description
QEMU agent for Windows guests offering the following features:

***FIXME***

%description -n mingw32-qemu-ga
QEMU agent for Windows guests offering the following features:

***FIXME***

%description -n mingw64-qemu-ga
Spice agent for Windows guests offering the following features:

***FIXME***


%prep
%setup -q -n qemu-%{version}

%{mingw_debug_package}

%global mingw32_configure %{mingw32_env} ; \
  __mingw32_topdir=.; if ! test -x configure; then __mingw32_topdir=..; fi; \\\
  $__mingw32_topdir/configure \\\
	--cross-prefix=%{mingw32_host}- \\\
	--prefix=%{mingw32_prefix} \\\
	--bindir=%{mingw32_bindir} \\\
	--sbindir=%{mingw32_sbindir} \\\
	--sysconfdir=%{mingw32_sysconfdir} \\\
	--datadir=%{mingw32_datadir} \\\
	--includedir=%{mingw32_includedir} \\\
	--libdir=%{mingw32_libdir} \\\
	--libexecdir=%{mingw32_libexecdir} \\\
	--localstatedir=%{mingw32_localstatedir} \\\
	--sharedstatedir=%{mingw32_sharedstatedir} \\\
	--mandir=%{mingw32_mandir} \\\
	--infodir=%{mingw32_infodir} \\\
	${MINGW_CONFIGURE_ARGS} \\\
	${MINGW32_CONFIGURE_ARGS}

%global mingw64_configure %{mingw64_env} ; \
  __mingw64_topdir=.; if ! test -x configure; then __mingw64_topdir=..; fi; \\\
  $__mingw64_topdir/configure \\\
	--cross-prefix=%{mingw64_host}- \\\
	--prefix=%{mingw64_prefix} \\\
	--bindir=%{mingw64_bindir} \\\
	--sbindir=%{mingw64_sbindir} \\\
	--sysconfdir=%{mingw64_sysconfdir} \\\
	--datadir=%{mingw64_datadir} \\\
	--includedir=%{mingw64_includedir} \\\
	--libdir=%{mingw64_libdir} \\\
	--libexecdir=%{mingw64_libexecdir} \\\
	--localstatedir=%{mingw64_localstatedir} \\\
	--sharedstatedir=%{mingw64_sharedstatedir} \\\
	--mandir=%{mingw64_mandir} \\\
	--infodir=%{mingw64_infodir} \\\
	${MINGW_CONFIGURE_ARGS} \\\
	${MINGW64_CONFIGURE_ARGS}

%build
%mingw_configure --enable-guest-agent --enable-guest-agent-msi
%mingw_make %{?_smp_mflags} qemu-ga.exe V=1
%mingw_make %{?_smp_mflags} msi V=1


%install
#mingw_make_install qemu-ga.exe msi DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{mingw32_bindir}
mkdir -p $RPM_BUILD_ROOT/%{mingw64_bindir}
mkdir -p $RPM_BUILD_ROOT/%{mingw32_datadir}/qemu-ga
mkdir -p $RPM_BUILD_ROOT/%{mingw64_datadir}/qemu-ga
cp build_win32/qemu-ga-i386.msi $RPM_BUILD_ROOT/%{mingw32_datadir}/qemu-ga/
cp build_win64/qemu-ga-x86_64.msi $RPM_BUILD_ROOT/%{mingw64_datadir}/qemu-ga/
cp build_win32/qemu-ga.exe $RPM_BUILD_ROOT/%{mingw32_bindir}/
cp build_win64/qemu-ga.exe $RPM_BUILD_ROOT/%{mingw64_bindir}/

%files -n mingw32-qemu-ga
%defattr(-,root,root)
%{mingw32_bindir}/qemu-ga.exe
%{mingw32_datadir}/qemu-ga/qemu-ga-i386.msi

%files -n mingw64-qemu-ga
%defattr(-,root,root)
%{mingw64_bindir}/qemu-ga.exe
%{mingw64_datadir}/qemu-ga/qemu-ga-x86_64.msi

%changelog
* Wed Nov 25 2015 Christophe Fergeau <cfergeau@redhat.com> 2.5.0-1
- Initial spec file - this builds the QEMU guest agent with mingw and its
  msi installer

